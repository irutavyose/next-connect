const mongoose = require('mongoose');
const User = mongoose.model('User');
const passport = require('passport');

exports.validateSignup = (req, res, next) => {
    req.sanitizeBody('name')
    req.sanitizeBody('email')
    req.sanitizeBody('password')

    // Name is no null and is between 4 characters and 10 characters

    req.checkBody('name', 'Entre votre nom ').notEmpty();
    req.checkBody('name', 'Le nom doit etre entre 4 et 10 caracteres ').isLength({ min: 4, max: 10 });

    // Email is no null, valid, and normaliz

    req.checkBody('email', 'Entre et valider votre mail').isEmail().isnormalizeEmail();

    // Name is no null and is between 4 characters and 10 characters 

    req.checkBody('password', 'Entre votre Password ').notEmpty();
    req.checkBody('password', 'Le Password doit etre entre 4 et 10 caracteres ').isLength({ min: 4, max: 10 });

    const errors = req.validationErrors()
    
    if(errors){

       const firstError = errors.map(error => error.msg)[0];
       
       return res.status(400).send(firstError)

    }

    next()

};

exports.signup = async (req, res,) => {

    const { name, email, password } = req.body;

    const user = await new User({ name, email, password })

    await user.register(user, password, (err, user) =>{
        if(err){
            return res.status(500).send(err.message);
        }
        res.json(user);
    })
};

exports.signin = (req, res, next) => {
    passport.authenticate('local', (err, user, info) =>{

        if(err){
            return res.status(500).json(err.message)
        }
        if(!user){
            return res.status(500).json(info.message)
        }

        req.logIn(user, err => {
            if (err) {
              return res.status(500).json(err.message)  
            }
            res.json(user)
        })
    })(req, res, next)
};

exports.signout = () => {};

exports.checkAuth = () => {};
